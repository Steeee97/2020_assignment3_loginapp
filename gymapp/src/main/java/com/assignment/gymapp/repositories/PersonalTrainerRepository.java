package com.assignment.gymapp.repositories;

import org.springframework.stereotype.Repository;

import com.assignment.gymapp.entities.PersonalTrainer;


@Repository
public interface PersonalTrainerRepository extends PersonRepository<PersonalTrainer>{
	
	PersonalTrainer findByUsername(String username);
}