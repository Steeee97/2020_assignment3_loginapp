package com.assignment.gymapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assignment.gymapp.entities.Plan;

@Repository
public interface PlanRepository<T extends Plan> extends CrudRepository<T, Long> {

}
