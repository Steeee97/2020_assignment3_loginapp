package com.assignment.gymapp.repositories;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.assignment.gymapp.entities.Customer;

@Repository
public interface CustomerRepository extends PersonRepository<Customer> {

	Iterable<Customer> findAll();
	
	Customer findByUsername(String username);
	
	Customer findById(long id);
	
	List<Customer> findByfirstNameIgnoreCaseContaining(String search);
	
	List<Customer> findBylastNameIgnoreCaseContaining(String search);


}