package com.assignment.gymapp.repositories;

import org.springframework.stereotype.Repository;
import com.assignment.gymapp.entities.Receptionist;

@Repository
public interface ReceptionistRepository extends PersonRepository<Receptionist> {

}
