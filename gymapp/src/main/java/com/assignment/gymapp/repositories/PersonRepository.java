package com.assignment.gymapp.repositories;

import com.assignment.gymapp.entities.Person;

import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface PersonRepository <T extends Person> extends CrudRepository<T, Long> {	

	Person findByUsername(String username);
	
	Person findById(long id);
	
	List<Person> findByFavourite(Person person);
}