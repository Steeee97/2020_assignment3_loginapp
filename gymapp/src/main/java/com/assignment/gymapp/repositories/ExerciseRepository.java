package com.assignment.gymapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.assignment.gymapp.entities.Exercise;

public interface ExerciseRepository <T extends Exercise> extends CrudRepository<T, Long>{

}
