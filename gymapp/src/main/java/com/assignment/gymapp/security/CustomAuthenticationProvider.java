package com.assignment.gymapp.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.assignment.gymapp.entities.Person;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private LoginService service;
	
    @Override
    public Authentication authenticate(Authentication auth) {
    	
        String username = auth.getName();
        String password = auth.getCredentials().toString();
        Person user = this.service.login(username, password);
        
        if (user != null) {
        	List<SimpleGrantedAuthority> role = new ArrayList<>();
        	switch(user.getRole()) {
        		case 1:
        			role.add(new SimpleGrantedAuthority("DEV"));
        			break;
        		case 2:
        			role.add(new SimpleGrantedAuthority("CUS"));
        			break;
        		case 3:
        			role.add(new SimpleGrantedAuthority("PT"));
        			break;
        		case 4:
        			role.add(new SimpleGrantedAuthority("REC"));
        			break;       			
        	}       		
        	UsernamePasswordAuthenticationToken  token = new UsernamePasswordAuthenticationToken(user.getUsername(), password, role);
        	return token;
        } else {
            throw new 
              BadCredentialsException("External system authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}