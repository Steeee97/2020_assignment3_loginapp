package com.assignment.gymapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
			
    @Autowired
    private CustomAuthenticationProvider authProvider;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }    
    
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/css/**", "/js/**").permitAll()
				.antMatchers("/", "/index").permitAll()
				.antMatchers("/creaScheda").hasAnyAuthority("PT", "DEV")
				.antMatchers("/impostazioni").hasAnyAuthority("REC", "DEV")
				.antMatchers("/scheda").hasAnyAuthority("CUS", "DEV")
				.antMatchers("/favorito").hasAnyAuthority("CUS", "DEV")
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/welcome", true)
				.permitAll()
				.and()
			.logout()
				.permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/403");
	}
}
