package com.assignment.gymapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.gymapp.entities.Person;
import com.assignment.gymapp.repositories.PersonRepository;

@Service
public class LoginService {
	
	@Autowired
	private PersonRepository<Person> repo;
	
	public Person login(String username, String password){
		
		Person returnUsername = this.repo.findByUsername(username);		
		
		if (returnUsername != null && returnUsername.getPassword().equals(password)){
			return returnUsername;
		} else 
			return null;
	}
}