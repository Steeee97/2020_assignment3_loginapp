package com.assignment.gymapp.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.assignment.gymapp.repositories.CustomerRepository;
import com.assignment.gymapp.repositories.ExerciseRepository;
import com.assignment.gymapp.repositories.PersonalTrainerRepository;
import com.assignment.gymapp.repositories.PlanRepository;
import com.assignment.gymapp.entities.Plan;
import com.assignment.gymapp.entities.Exercise;
import com.assignment.gymapp.entities.Person;
import com.assignment.gymapp.entities.PersonalTrainer;
import com.assignment.gymapp.entities.Customer;

@Service
public class PlanService {
	
	@Autowired
	private CustomerRepository repo;
	@Autowired
	private PlanRepository<Plan> planRepo;
	@Autowired
	private ExerciseRepository<Exercise> exerciseRepo;
	@Autowired
	private PersonalTrainerRepository ptRepo;
	
	public List<Exercise> getExercises(String custUsername) {
		List<Exercise> finalList = null;
		Customer cust = this.repo.findByUsername(custUsername);
		if(cust != null) {
			Plan piano = cust.getPlan();
			if (piano != null) {
				finalList = new ArrayList<>(piano.getExercises());
				return finalList;
			} 			
			finalList = new ArrayList<>();			
		}
		return finalList;
	}
	
	public List<Plan> getPlans(){
		return (List<Plan>) planRepo.findAll();		
	}
	
	public List<Exercise> getExercises(){
		return (List<Exercise>) exerciseRepo.findAll();
	}
	
	public Person getCustomer(String user) {
		return repo.findByUsername(user);
	}
	
	public Customer getCustomerById(int id) {
		return repo.findById((long) id);
	}
	
	public List<Customer> getAllCustomer() {
		return (List<Customer>) repo.findAll();
	}
	
	public void saveCustomer(Customer cust) {
		repo.save(cust);
	}
	
	public Plan getPlan(String custUsername){
		Plan plan = null;
		Customer cust = this.repo.findByUsername(custUsername);
		if(cust != null) {
			plan = cust.getPlan();
			
			if(plan != null)
				return plan;
		}
		
		return plan;
	}
	
	public PersonalTrainer getPTByUsername(String user) {
		return (PersonalTrainer) this.ptRepo.findByUsername(user);
	}
	
	public void save(Plan plan) {
		planRepo.save(plan);
	}
	
	public Plan getPlanById(int id) {
		return planRepo.findById((long) id).get();
	}
	
	public Exercise getExerciseById(int id) {
		return exerciseRepo.findById((long) id).get();
	}
}
