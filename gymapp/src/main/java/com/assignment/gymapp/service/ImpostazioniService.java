package com.assignment.gymapp.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.gymapp.entities.Customer;
import com.assignment.gymapp.entities.Person;
import com.assignment.gymapp.repositories.CustomerRepository;
import com.assignment.gymapp.repositories.PersonRepository;

@Service
public class ImpostazioniService {
	@Autowired
	private CustomerRepository repo;
	@Autowired
	private PersonRepository<Person> repoP;
	
	public List<Customer> getAllPeople(){
		return (List<Customer>) repo.findAll();		
	}
	
	public List<Customer> searchPeople(String search){
		List<Customer> firstname = repo.findByfirstNameIgnoreCaseContaining(search);
		List<Customer> lastname = repo.findBylastNameIgnoreCaseContaining(search);
		
		for(Customer account : firstname) {
			if(!lastname.contains(account)) {
				lastname.add(account);
			}
		}
		
		return lastname;
	}
	
	public void deleteCustomer(int id) {
	    Customer cust = repo.findById((long) id);
	    cust.setPlan(null);
	    
	    Person per = (Person) repoP.findById((long) id);
	    if(per != null) {
		    List<Person> cust2 = repo.findByFavourite(per);
		    
		    for(Person tempCust : cust2) {
		    	tempCust.setFavourite(null);
		    }	
	    }
	    repo.deleteById((long) id);
	  }
	
	public Customer getCustomerById(long id){
		return repo.findById(id);		
	}
	
	public Customer getCustomerByUser(String user) {
		return repo.findByUsername(user);
	}
	
	public void save(Customer person){		
		repo.save(person);		
	}	
	
}
