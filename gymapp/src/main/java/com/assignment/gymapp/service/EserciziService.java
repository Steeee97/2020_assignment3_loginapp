package com.assignment.gymapp.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.gymapp.entities.Exercise;
import com.assignment.gymapp.repositories.ExerciseRepository;


@Service
public class EserciziService {
	@Autowired
	private ExerciseRepository<Exercise> repo;
		
	public List<Exercise> getExercises(){
		return (List<Exercise>) repo.findAll();
	}
		
	public void save(Exercise ex) {
		this.repo.save(ex);
	}
	

}
