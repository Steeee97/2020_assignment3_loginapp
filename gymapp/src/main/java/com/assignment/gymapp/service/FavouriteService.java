package com.assignment.gymapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.gymapp.entities.Customer;
import com.assignment.gymapp.entities.Person;
import com.assignment.gymapp.repositories.CustomerRepository;
import com.assignment.gymapp.repositories.PersonRepository;

@Service
public class FavouriteService {

	@Autowired
	private CustomerRepository repo;
	@Autowired
	private PersonRepository<Person> repoP;
	
	public List<Customer> getAllPeople(){
		return (List<Customer>) repo.findAll();		
	}
	
	public Person getCustomer(String user) {
		return repoP.findByUsername(user);
	}
	
	public Person getFavourite(int id) {
		return repoP.findById((long) id);
	}
	
	public Person save(Person person) {
		return repoP.save(person);
	}
}
