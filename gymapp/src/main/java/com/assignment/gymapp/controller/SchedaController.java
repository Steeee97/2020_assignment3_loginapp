package com.assignment.gymapp.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.assignment.gymapp.service.PlanService;
import com.assignment.gymapp.entities.Exercise;

@Controller
public class SchedaController {
	@Autowired
	private PlanService service;

  @RequestMapping(value = "/scheda", method = RequestMethod.GET)
  public ModelAndView scheda(HttpServletRequest request, HttpServletResponse response) {
	  String userName = request.getRemoteUser();
	  List<Exercise> scheda = this.service.getExercises(userName);

	  ModelAndView mav = new ModelAndView("scheda");
	  mav.addObject("listaEsercizi", scheda);
	  
	  return mav;
  }
}
