package com.assignment.gymapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.assignment.gymapp.entities.Customer;
import com.assignment.gymapp.entities.Person;
import com.assignment.gymapp.service.FavouriteService;

import java.util.List;

@Controller
public class FavouriteController {
	@Autowired
	private FavouriteService service;
	
	@RequestMapping(value = "/favorito", method = RequestMethod.GET)
	public ModelAndView favourite(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = null;
		 Person me = this.service.getCustomer(request.getRemoteUser());
		 if(me.getFavourite() != null) {
			  mav = new ModelAndView("watchFavorito");
			  Person favourite = me.getFavourite();
			  mav.addObject("favourite", favourite);		
		  }else {
			  mav = new ModelAndView("setFavorito");
			  List<Customer> people = this.service.getAllPeople();
			  mav.addObject("people", people);		  
		  }	  
		  return mav;
	 }
	
	@RequestMapping("/setFavourite/{id}")
	public String setFavourite(HttpServletRequest request, HttpServletResponse response, @PathVariable(name = "id") int id) {				
		Person favourite = service.getFavourite(id);
		Person me = this.service.getCustomer(request.getRemoteUser());		
		me.setFavourite(favourite);
		this.service.save(me);
		
		return "redirect:/favorito";
	}
	
	@RequestMapping("/changeFavourite")
	public String changeFavourite(HttpServletRequest request, HttpServletResponse response) {				
		Person me = this.service.getCustomer(request.getRemoteUser());		
		me.setFavourite(null);
		this.service.save(me);
		
		return "redirect:/favorito";
	}
}
