package com.assignment.gymapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.assignment.gymapp.entities.Customer;
import com.assignment.gymapp.service.ImpostazioniService;

@Controller
public class ImpostazioniController {
  @Autowired
  private ImpostazioniService service;

  @RequestMapping(value = "/impostazioni", method = RequestMethod.GET)
  public ModelAndView impostazioni(HttpServletRequest request, HttpServletResponse response) {
      List<Customer> accounts = this.service.getAllPeople();  
      String search = null;
    
      ModelAndView mav = new ModelAndView("impostazioni");
      mav.addObject("listAccounts", accounts);
      mav.addObject("search", search);

      return mav;
   }
  
  @RequestMapping("/editAccount/{id}")
  public ModelAndView showEditAccountForm(@PathVariable(name = "id") int id) {        
    Customer account = this.service.getCustomerById((long) id);
    
    ModelAndView mav = new ModelAndView("editAddAccount");
    mav.addObject("account", account);    
    mav.addObject("add", false);
    
    return mav;
  }
  
  @RequestMapping("/addAccount")
  public ModelAndView showAddAccountForm() {        
    Customer account = new Customer();
    
    ModelAndView mav = new ModelAndView("editAddAccount");
    mav.addObject("account", account);
    
    mav.addObject("add", true);
    
    return mav;
  }
  
  @RequestMapping(value ="/deleteAccount/{id}")
  public String deleteAccount(@PathVariable(name = "id") int id) throws Exception {
    try {
      this.service.deleteCustomer(id);
    }catch(Exception ex) {
      throw new Exception("Attenzione!!! L'utente ha una scheda associta oppure ... pertanto non puo essere eliminato. "+ ex);
    }
    return "redirect:/impostazioni";
  }
  
  @RequestMapping(value = "/save/{id}", method = RequestMethod.POST)
  public String saveAccount(@ModelAttribute("account") Customer account, @PathVariable(name = "id") int id) throws Exception {    
    try {
      if(id == 0) {
        account.setRole(2);
        this.service.save(account);
      }else {
        account.setId((long) id);
        
        if(account.getPassword() == "") {
          Customer edit = this.service.getCustomerById(id);
          String password = edit.getPassword();
          account.setRole(edit.getRole());
          account.setPassword(password);
        }            
        this.service.save(account);
      }
    }catch(Exception ex) {
      throw new Exception("Attenzione!!! Username gia' presente, provare a metterne un altro! oppure "+ ex);
    }
    return "redirect:/impostazioni";
  }
  
  @RequestMapping(value = "/searchAccount", method = RequestMethod.GET)
  public ModelAndView searchAccount(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("search") String search) {
    
      List<Customer> accounts = this.service.searchPeople(search);  
    
      ModelAndView mav = new ModelAndView("impostazioni");
      mav.addObject("listAccounts", accounts);
      mav.addObject("search", search);

      return mav;
   }
  

}