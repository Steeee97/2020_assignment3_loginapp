package com.assignment.gymapp.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.assignment.gymapp.service.EserciziService;
import com.assignment.gymapp.entities.Exercise;

@Controller
public class EserciziController {
	@Autowired
	private EserciziService service;

  @RequestMapping(value = "/esercizi", method = RequestMethod.GET)
  public ModelAndView esercizi(HttpServletRequest request, HttpServletResponse response) {
	  List<Exercise> esercizi = this.service.getExercises();
	  ModelAndView mav = new ModelAndView("esercizi");
	  mav.addObject("esercizi", esercizi);
	  return mav;
  }
  
	@RequestMapping("/addEsercizio")
	public ModelAndView addScheda() {				
		Exercise ex = new Exercise();		
		ModelAndView mav = new ModelAndView("addEsercizio");
		mav.addObject("esercizio", ex);
		
		return mav;
	}
	
	
	@RequestMapping(value ="/saveEsercizio", method = RequestMethod.POST)
	public String saveScheda(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("esercizio") Exercise ex) {	

		this.service.save(ex);
		
		return "redirect:/esercizi";
	}
}
