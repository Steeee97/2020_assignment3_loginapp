package com.assignment.gymapp.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.assignment.gymapp.service.PlanService;
import com.assignment.gymapp.entities.Customer;
import com.assignment.gymapp.entities.Exercise;
import com.assignment.gymapp.entities.PersonalTrainer;
import com.assignment.gymapp.entities.Plan;

@Controller
public class CreaPianoController {
	@Autowired
	private PlanService service;

  @RequestMapping(value = "/creaScheda", method = RequestMethod.GET)
  public ModelAndView creaScheda(HttpServletRequest request, HttpServletResponse response) {
	  List<Plan> piani = this.service.getPlans();
	  ModelAndView mav = new ModelAndView("creaScheda");
	  mav.addObject("piani", piani);
	  return mav;
  }
  
	@RequestMapping("/addScheda")
	public ModelAndView addScheda() {				
		Plan piano = new Plan();
		
		ModelAndView mav = new ModelAndView("addScheda");
		mav.addObject("piano", piano);
		
		return mav;
	}
	
	@RequestMapping("/completaScheda/{id}")
	public ModelAndView completaScheda(@PathVariable(name = "id") int id) {				
		Plan piano = this.service.getPlanById(id);
		List<Exercise> esercizi = this.service.getExercises(); 
		
		ModelAndView mav = new ModelAndView("completaScheda");
		mav.addObject("piano", piano);
		mav.addObject("esercizi", esercizi);
		mav.addObject("plan_id", id);
		
		return mav;
	}
	
	@RequestMapping("/addExeScheda")
	public String addExeScheda(@RequestParam(value = "id", required = true) int id, @RequestParam(value = "plan_id", required = true) int plan_id) {				
		Plan piano = this.service.getPlanById(plan_id);
		Exercise esercizio = this.service.getExerciseById(id); 
		
		piano.setExercise(esercizio);
		this.service.save(piano);
		
		return "redirect:/completaScheda/"+plan_id;
	}
	
	@RequestMapping(value ="/saveScheda", method = RequestMethod.POST)
	public String saveScheda(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("piano") Plan piano) {	
		PersonalTrainer me = this.service.getPTByUsername(request.getRemoteUser());
		piano.setPersonal(me);
		this.service.save(piano);
		
		return "redirect:/creaScheda";
	}
	
	@RequestMapping("/assegnaScheda/{id}")
	public ModelAndView assegnaScheda(@PathVariable(name = "id") int id) {				
		List<Customer> customer = this.service.getAllCustomer();
		List<Customer> assCust = new ArrayList<Customer>();
		System.out.println(customer);
		for(int i=0; i< customer.size(); i++) {
			if(customer.get(i).getPlan() != null) {
				if(customer.get(i).getPlan().getPlan_id() == (long) id) {
					assCust.add(customer.get(i));
				}
				customer.remove(i);
				i--;
			}
		}
		System.out.println(assCust);
		ModelAndView mav = new ModelAndView("assegnaScheda");
		mav.addObject("customer", customer);
		mav.addObject("AssoCustomer", assCust);
		mav.addObject("plan_id", id);
		
		return mav;
	}
	
	@RequestMapping("/addCustScheda")
	public String addCustScheda(@RequestParam(value = "id", required = true) int id, @RequestParam(value = "plan_id", required = true) int plan_id) {				
		Plan piano = this.service.getPlanById(plan_id);
		Customer cust = this.service.getCustomerById(id); 
		cust.setPlan(piano);
		
		this.service.saveCustomer(cust);
		
		return  "redirect:/assegnaScheda/"+plan_id;
	}
}
