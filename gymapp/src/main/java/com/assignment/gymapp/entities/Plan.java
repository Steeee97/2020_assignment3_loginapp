package com.assignment.gymapp.entities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

@Entity
@Table(name = "plan")
public class Plan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, name = "plan_id")
	private Long plan_id;
	
	@NotNull
	@Size(min = 1, max = 20, message = "The name of type plan must be between 1 and 20 charcaters long")
	@Column(name = "type_plan")
	private String typePlan;
	
	@NotNull
	@Column(name = "estimated_time")
	private int estimatedTime;
	
	@NotNull
	@Column(name = "how_time")
	private int howTime;
	
	@NotNull
	@Column(name = "days_to_week")
	private int daysToWeek;
		
	@ManyToOne
	@JoinColumn(name = "personal_trainer_id", referencedColumnName = "personal_trainer_id")
	private PersonalTrainer personal;
	
	@ManyToMany(cascade={CascadeType.ALL})
	@JoinTable(name="Exercise_Plan",
		joinColumns={@JoinColumn(name = "plan_id", referencedColumnName = "plan_id")},
		inverseJoinColumns={@JoinColumn(name = "exercise_id", referencedColumnName = "exercise_id")})
	private Set<Exercise> exercises = new HashSet<Exercise>();
	
	@OneToMany(mappedBy = "plan")
	private List<Customer> customer;
	
	public Plan() {
		
	}

	public Plan(Long plan_id,
			@NotNull @Size(min = 1, max = 20, message = "The name of type plan must be between 1 and 20 charcaters long") String typePlan,
			@NotNull int estimatedTime,
			@NotNull int howTime,
			@NotNull int daysToWeek) {
		
		this.plan_id = plan_id;
		this.typePlan = typePlan;
		this.estimatedTime = estimatedTime;
		this.howTime = howTime;
		this.daysToWeek = daysToWeek;
	}

	public Long getId() {
		return this.plan_id;
	}
	
	public String getTypePlan() {
		return typePlan;
	}

	public void setTypePlan(String typePlan) {
		this.typePlan = typePlan;
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public int getHowTime() {
		return howTime;
	}

	public void setHowTime(int howTime) {
		this.howTime = howTime;
	}

	public int getDaysToWeek() {
		return daysToWeek;
	}

	public void setDaysToWeek(int daysToWeek) {
		this.daysToWeek = daysToWeek;
	}

	public PersonalTrainer getPersonal() {
		return personal;
	}
	
	public void setPersonal(PersonalTrainer pt) {
		this.personal = pt;
	}

	public Long getPlan_id() {
		return plan_id;
	}
	
	public Set<Exercise> getExercises() {
		return exercises;
	}

	public void setExercise(Exercise exercise) {
		this.exercises.add(exercise);
	}

	public List<Customer> getCustomer() {
		return customer;
	}	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((plan_id == null) ? 0 : plan_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plan other = (Plan) obj;
		if (plan_id == null) {
			if (other.plan_id != null)
				return false;
		} else if (!plan_id.equals(other.plan_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plan [ Type plan=" + typePlan + ", estimated time=" + estimatedTime + ", how time="
				+ howTime + ", days to week=" + daysToWeek + "]";		
	}
}
