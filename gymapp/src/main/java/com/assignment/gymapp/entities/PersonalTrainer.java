package com.assignment.gymapp.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity(name = "personal_trainer")
@PrimaryKeyJoinColumn(name = "personal_trainer_id")
public class PersonalTrainer extends Person {
	
	@NotNull
	@Column(name = "years_experiences_personal")
	private Integer yearsExp;

	@OneToMany(mappedBy = "personal")
	protected Set<Plan> plans = new HashSet<>();

	public PersonalTrainer() {
		super();
		
	}

	public PersonalTrainer(
			@NotNull @Size(min = 1, max = 50, message = "The first name must be between 1 and 50 charcaters long") String firstName,
			@NotNull @Size(min = 1, max = 50, message = "The first lastname must be between 1 and 50 charcaters long") String lastName,
			@NotNull @Pattern(regexp = "[\\d]{10}") String nPhone,
			@NotNull @Email String mail,
			@NotNull(message = "The username field cannot be empty.") @Size(min = 5, message = "The username field must have at least five character.") String username,
			@NotNull(message = "The password field cannot be empty.") @Size(min = 8, message = "The password must be at least 8 characters long.") String password,
			@NotNull int role,
			@NotNull Integer yearsExp) {
		super(firstName, lastName, nPhone, mail, username, password, role);
		this.yearsExp = yearsExp;
	}
	
	public Integer getYearsExp() {
		return yearsExp;
	}

	public void setYearsExp(Integer yearsExp) {
		this.yearsExp = yearsExp;
	}

	public Set<Plan> getPlans() {
		return plans;
	}
	
	public void setPlans(Set<Plan> plans) {
		this.plans = plans;
	}

	@Override
	public String toString() {
		return "PersonalTrainer [yearsExp=" + yearsExp + ", plans=" + /*plans + */"]" + super.toString();
	}
	
	
}