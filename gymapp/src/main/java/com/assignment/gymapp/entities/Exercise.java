package com.assignment.gymapp.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "exercise")
public class Exercise {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, name = "exercise_id")
	private long exercise_id;
	
	@NotNull
	@Size(min = 1, max = 30, message = "The name of exercise must be between 1 and 30 charcaters long")
	@Column(name = "name_exercise")
	private String name;
	
	@NotNull
	@Column(name = "number_repetition")
	private int nRepetition;
	
	@NotNull
	@Column(name = "number_serie")
	private int nSerie;
	
	@NotNull
	@Column(name = "break_time")
	private int breakTime;
	
	
	@Column(length = 50, name = "description")
	private String description;
	
	@Column(name = "weight")
	private int weight;
	
	@ManyToMany(mappedBy = "exercises")
	private Set<Plan> plans = new HashSet<Plan>();
	
	public Exercise() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Exercise(
			@NotNull @Size(min = 1, max = 30, message = "The name of exercise must be between 1 and 30 charcaters long") String name,
			@NotNull @Size(min = 5, max = 30, message = "The number of repetitions must be between 5 and 30") @Pattern(regexp = "[\\d]{3}") int nRepetition,
			@NotNull @Size(min = 1, max = 6, message = "The number of serie must be between 1 and 6") @Pattern(regexp = "[\\d]{2}") int nSerie,
			@NotNull int breakTime, String description, int weight) {
		super();
		this.name = name;
		this.nRepetition = nRepetition;
		this.nSerie = nSerie;
		this.breakTime = breakTime;
		this.description = description;
		this.weight = weight;
	}

	public long getExercise_id() {
		return exercise_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getnRepetition() {
		return nRepetition;
	}

	public void setnRepetition(int nRepetition) {
		this.nRepetition = nRepetition;
	}

	public int getnSerie() {
		return nSerie;
	}

	public void setnSerie(int nSerie) {
		this.nSerie = nSerie;
	}

	public int getBreakTime() {
		return breakTime;
	}

	public void setBreakTime(int breakTime) {
		this.breakTime = breakTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Set<Plan> getPlans() {
		return plans;
	}

	@Override
	public String toString() {
		return "Exercise [excersise_id=" + exercise_id + ", name=" + name + ", nRepetition=" + nRepetition
				+ ", nSerie=" + nSerie + ", breakTime=" + breakTime + ", description=" + description + "]";
	}
	
	
}
