package com.assignment.gymapp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity(name = "receptionist")
@PrimaryKeyJoinColumn(name = "receptionist_id")
public class Receptionist extends Person {
	
	@NotNull
	@Column(name = "years_experiences_receptionist")
	private Integer yearsExp;

	
	public Receptionist() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Receptionist(
			@NotNull @Size(min = 1, max = 50, message = "The first name must be between 1 and 50 charcaters long") String firstName,
			@NotNull @Size(min = 1, max = 50, message = "The first lastname must be between 1 and 50 charcaters long") String lastName,
			@NotNull @Pattern(regexp = "[\\d]{10}") String nPhone,
			@NotNull @Email String mail,
			@NotNull(message = "The username field cannot be empty.") @Size(min = 5, message = "The username field must have at least five character.") String username,
			@NotNull(message = "The password field cannot be empty.") @Size(min = 8, message = "The password must be at least 8 characters long.") String password,
			@NotNull int role,
			@NotNull Integer yearsExp) {
		super(firstName, lastName, nPhone, mail, username, password, role);
		this.yearsExp = yearsExp;
	}

	public Integer getYearsExp() {
		return yearsExp;
	}

	public void setYearsExp(Integer yearsExp) {
		this.yearsExp = yearsExp;
	}


}