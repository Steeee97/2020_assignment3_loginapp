package com.assignment.gymapp.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity(name = "customer")
@PrimaryKeyJoinColumn(name = "customer_id")
public class Customer extends Person {
	
	@NotNull(message = "Please, provide a valid age.") 
	@Min(value = 16, message = "You must be at least sixteen to sign up.") 
	@Column(name = "age")
	private Integer age;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "plan_id", referencedColumnName = "plan_id")
	private Plan plan;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(
			@NotNull @Size(min = 1, max = 50, message = "The first name must be between 1 and 50 charcaters long") String firstName,
			@NotNull @Size(min = 1, max = 50, message = "The first lastname must be between 1 and 50 charcaters long") String lastName,
			@NotNull @Pattern(regexp = "[\\d]{10}") String nPhone,
			@NotNull @Email String mail,
			@NotNull(message = "The username field cannot be empty.") @Size(min = 5, message = "The username field must have at least five character.") String username,
			@NotNull(message = "The password field cannot be empty.") @Size(min = 8, message = "The password must be at least 8 characters long.") String password,
			@NotNull int role,
			@NotNull(message = "Please, provide a valid age.") @Min(value = 16, message = "You must be at least sixteen to sign up.") Integer age ) {
		super(firstName, lastName, nPhone, mail, username, password, role);
		this.age = age;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	@Override
	public String toString() {
		return "Customer [age=" + age + ", plan=" + /*plan + */"]" + super.toString();
	}
	
	

}