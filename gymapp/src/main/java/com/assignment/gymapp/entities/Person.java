package com.assignment.gymapp.entities;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="person")
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, name = "person_id")
	private long person_id;
	

	@NotNull
	@Size(min = 1, max = 50, message = "The first name must be between 1 and 50 charcaters long")
	@Column(length = 50, name = "first_name")
	protected String firstName;
	
	@NotNull
	@Size(min = 1, max = 50, message = "The last name must be between 1 and 50 charcaters long")
	@Column(length = 50, name = "last_name")
	protected String lastName;
	
	@NotNull
	@Pattern(regexp="[\\d]{10}")
	@Column(length = 10, name = "phone_number")
	protected String nPhone;
	
	@Email
	@NotNull
	@Column(length = 200, name = "email")
	protected String mail;	
	
	@NotNull(message = "The username field cannot be empty.")
	@Size(min = 5, message = "The username must be at least 5 characters long.")
	//@UniqueUsername(message = "This username is not available")
	@Column(unique = true, name= "username")
	protected String username;
	
	
	@NotNull(message = "The password field cannot be empty.")
	@Size(min = 8, message = "The password must be at least 8 characters long.")
	@Column(name= "password")
	protected String password;
	
	@NotNull
	@Column(name = "role")
	protected int role;
	
	//La persona preferita dell'utente
	@ManyToOne
	@JoinColumn(name="favourite_id")
	private Person favourite;
	
	//Utenti che hanno messo quel utente come preferito
	@OneToMany(mappedBy="favourite", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	protected Set<Person> favourites = new HashSet<>();
	
	public Person() {
		//
	}	

	public Person(
			@NotNull @Size(min = 1, max = 50, message = "The first name must be between 1 and 50 charcaters long") String firstName,
			@NotNull @Size(min = 1, max = 50, message = "The last name must be between 1 and 50 charcaters long") String lastName,
			@NotNull @Pattern(regexp = "[\\d]{10}") String nPhone,
			@Email @NotNull String mail,
			@NotNull(message = "The username field cannot be empty.") @Size(min = 5, message = "The username must be at least 5 characters long.") String username,
			@NotNull(message = "The password field cannot be empty.") @Size(min = 8, message = "The password must be at least 8 characters long.") String password,
			@NotNull int role) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.nPhone = nPhone;
		this.mail = mail;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public long getId() {
		return person_id;
	}
	
	public void setId(long id) {
		this.person_id = id;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getnPhone() {
		return nPhone;
	}

	public void setnPhone(String nPhone) {
		this.nPhone = nPhone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public Person getFavourite() {
		return favourite;
	}

	public void setFavourite(Person favourite) {
		this.favourite = favourite;
	}

	public Set<Person> getFavourites() {
		return favourites;
	}

	public void setFavourites(Set<Person> favourites) {
		this.favourites = favourites;
	}
	
	
	@Override
	public String toString() {
		return "Person [id="+person_id+", firstName=" + firstName + ", lastName=" + lastName + ", nPhone=" + nPhone
				+ ", mail=" + mail + ", username=" + username + ", password=HIDDEN]";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (person_id != other.person_id)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (person_id ^ (person_id >>> 32));
		return result;
	}
	
	
}