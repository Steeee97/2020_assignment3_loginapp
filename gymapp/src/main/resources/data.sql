INSERT INTO person (person_id, first_name, last_name, email, phone_number, password, role, username)
VALUES (30, 'Kenji', 'Inafune', 'Inafune@gmail.com', '1234567890', 'passCliente', 2, 'userCliente')
ON DUPLICATE KEY UPDATE first_name = 'Kenji';

INSERT INTO person (person_id, first_name, last_name, email, phone_number, password, role, username)
VALUES (31, 'Kevin', 'Goodman', 'goodman@prova.it', '3234567890', 'passPersonal', 3, 'userPersonal')
ON DUPLICATE KEY UPDATE first_name = 'Kevin';

INSERT INTO person (person_id, first_name, last_name, email, phone_number, password, role, username)
VALUES (32, 'Billy', 'Bat', 'billybat@libero.it', '2234567890', 'passReceptionist', 4, 'userReceptionist')
ON DUPLICATE KEY UPDATE first_name = 'Billy';



INSERT INTO personal_trainer (years_experiences_personal, personal_trainer_id)
VALUES (2, 31)
ON DUPLICATE KEY UPDATE years_experiences_personal = 2;


INSERT INTO receptionist (years_experiences_receptionist, receptionist_id)
VALUES (3, 32)
ON DUPLICATE KEY UPDATE years_experiences_receptionist = 3;



INSERT INTO plan (plan_id, estimated_time, type_plan, how_time, days_to_week, personal_trainer_id)
VALUES (30, 40, 'potenziamento', 30, 4, 31)
ON DUPLICATE KEY UPDATE estimated_time = 40;


INSERT INTO customer (age, customer_id, plan_id)
VALUES (23, 30, 30)
ON DUPLICATE KEY UPDATE age = 23;


INSERT INTO exercise (exercise_id, break_time, description, number_repetition, number_serie, name_exercise, weight)
VALUES (50, 2, 'solo bicipite destro', 3, 3, 'curl con manubri', '10')
ON DUPLICATE KEY UPDATE weight = 10;

INSERT INTO exercise_plan (plan_id, exercise_id)
VALUES (30, 50)
ON DUPLICATE KEY UPDATE plan_id = 30;
