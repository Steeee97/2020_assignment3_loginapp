# Assignment 3 - Implementazione di una WebApp per la Palestra

prodotto da:

- Pievaioli Stefano, matricola 816592
- Presot Federico Davide, matricola 817290
- Bryan Ivan Zighui Guerrero, matricola 816335

## Descrizione dell'applicazione
Per il terzo progetto abbiamo deciso di creare una applicazione da utilizzare per la palestra; Questa applicazione è stata da noi pensata come supporto elettronico nella gestione degli allenamenti dei clienti iscritti alla palestra stessa, e di conseguenza è stata data una maggiore priorità alla gestione dell’allenamento stesso. L’applicazione presenta diversi tipi di utenti (receptionist, personal trainer e clienti) e a ciascuno di essi vengono fornite diverse funzionalità dell’applicazione stessa. Il riconoscimento del ruolo di ciascun utente viene effettuato tramite una funzione di login, che permette a ciascun utente di avere a disposizione solo i propri dati.

Per quanto riguarda la parte di front-end, abbiamo considerato l'utilizzo di Thymeleaf, mentre abbiamo considerato per la parte di back-end l'utilizzo di Hibernate e di Spring JPA per la generazione del database e per implementare la permanenza dei dati; inoltre, essi saranno estremamente utili per l'implementazione delle funzionalità CRUD nella nostra applicazione.

Per quanto riguarda lo schema della nostra applicazione, abbiamo già generato un possibile schema ER, in modo da avere già una possibile idea su come possa essere implementata la nostra applicazione; questa verrà poi modificata nel momento in cui l'Applicazione sarà effettivamente completata.

Maggiori informazioni sono disponibili all'interno della nostra relazione.
## Credenziali per l'accesso
Poiché abbiamo inserito una parte iniziale di login all'interno della nostra applicazione, si è resa necessaria la generazione di account iniziali disponibili popolando subito il database, in modo non solo da poter accedere all'applicazione stessa, ma anche per raggiungere più velocemente le funzionalità disponibili per ogni account. Per questo motivo, all'inizio dell'esecuzione dell'applicazione verrà popolato il database con tre account differenti, ciascuno con un ruolo diverso (da Customer, da Receptionist o da Personal Trainer). Per poter accedere, sono presenti le seguenti credeniali:

- Customer
```bash
Username: userCliente
Password: passCliente
```
- Receptionist
```bash
Username: userReceptionist
Password: passReceptionist
```
- Personal Trainer
```bash
Username: userPersonal
Password: passPersonal
```

## Installazione

Per fare partire l'applicazione, abbiamo pensato all'utilizzo di Docker; ciò è stato pensato per gestire con facilità il deploy della applicazione. Di conseguenza, è necessario che l'utente abbia Docker Compose installato sul proprio computer. Per far partire l'applicazione, sarebbe necessario di conseguenza effettuare il processo di build
 
```bash
docker-compose build
```
Per fare poi partire l'applicazione, basterà digitare

```bash
docker-compose up
```

Una volta fatta partire l'applicazione, essa sarà disponibile a http://localhost:8080
Durante la prima esecuzione delle fasi di build e di up l'applicazione potrebbe richiedere un po di tempo; ciò è da considerarsi normale. In particolar modo, la prima esecuzione della applicazione potrebbe richiedere una quantità maggiore di tempo, in quanto non solo deve essere generato il database, ma esso deve essere anche popolato.
